module StringMap = Map.Make (String)

let char_list_of_string s = s |> String.to_seq |> List.of_seq
let path_start = "AAA"
let path_end = "ZZZ"

let load_input filename =
  let lines =
    List.filter_map
      (fun s -> if s <> "" then Some s else None)
      In_channel.(with_open_text filename input_lines)
  in
  let path = char_list_of_string (List.hd lines) in
  let map =
    List.fold_left
      (fun acc l ->
        let d = String.split_on_char '=' l in
        let node = String.trim (List.nth d 0) in
        let childs = String.trim (List.nth d 1) in
        let left = String.sub childs 1 3
        and right = String.sub childs 6 3 in
        acc |> StringMap.add node (left, right))
      StringMap.empty
      (List.tl lines)
  in
  path, map
;;

let walk path map ?(cond = String.equal path_end) start =
  let rec loop4 acc next = function
    | [] -> loop4 acc next path
    | way :: tl ->
      let left, right = StringMap.find next map in
      if cond next
      then acc
      else (
        match way with
        | 'L' -> loop4 (acc + 1) left tl
        | 'R' -> loop4 (acc + 1) right tl
        | _ -> failwith "unknown route")
  in
  loop4 0 start path
;;

let part1 filename =
  let path, map = load_input filename in
  walk path map path_start
;;

(* Greater Common Denominator *)
let rec gcd u v = if v <> 0 then gcd v (u mod v) else abs u

(* Least Common Multiplier *)
let lcm m n =
  match m, n with
  | 0, _ | _, 0 -> 0
  | m, n -> abs (m * n) / gcd m n
;;

let part2 filename =
  let path, map = load_input filename in
  let starts =
    List.filter
      (String.ends_with ~suffix:"A")
      (List.map (fun (k, _) -> k) (StringMap.to_list map))
  in
  let walk' = walk path map ~cond:(String.ends_with ~suffix:"Z") in
  let paths = List.map walk' starts in
  List.fold_left lcm (List.hd paths) (List.tl paths)
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day08.test");
  Printf.printf " test: %d\n" (part1 "input/day08.test2");
  Printf.printf " input: %d\n" (part1 "input/day08.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day08.test3");
  Printf.printf " input: %d\n" (part2 "input/day08.input")
;;
