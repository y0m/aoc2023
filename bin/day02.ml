type cube =
  | Red of int
  | Green of int
  | Blue of int

let cubesmax = [ Red 12; Green 13; Blue 14 ]

let maxcolor = function
  | Red prev, Red next -> Red (max prev next)
  | Green prev, Green next -> Green (max prev next)
  | Blue prev, Blue next -> Blue (max prev next)
  | _ -> failwith "impossible color tuple"
;;

let rec maxcolors r g b = function
  | [] -> [ r; g; b ]
  | x :: xs ->
    (match x with
     | Red _ -> maxcolors (maxcolor (r, x)) g b xs
     | Green _ -> maxcolors r (maxcolor (g, x)) b xs
     | Blue _ -> maxcolors r g (maxcolor (b, x)) xs)
;;

let create_cube = function
  | [] -> failwith "empty cube"
  | [ c; "red" ] -> Red (int_of_string c)
  | [ c; "green" ] -> Green (int_of_string c)
  | [ c; "blue" ] -> Blue (int_of_string c)
  | [ a; b ] -> failwith (b ^ " " ^ a)
  | a -> failwith (List.nth a 0 ^ List.nth a 1)
;;

let rec create_set acc = function
  | [] -> acc
  | x :: xs ->
    create_set (create_cube (String.split_on_char ' ' (String.trim x)) :: acc) xs
;;

let rec create_sets acc = function
  | [] -> acc
  | x :: xs -> create_sets (create_set [] (String.split_on_char ',' x) :: acc) xs
;;

let rec game_id = function
  | [] -> failwith "no game info"
  | [ id ] -> id
  | _ :: xs -> game_id xs
;;

let rec create_game (id : int) (sets : cube list list) = function
  | [] -> id, sets
  | [ s ] -> create_game id (create_sets sets (String.split_on_char ';' s)) []
  | g :: s -> create_game (int_of_string (game_id (String.split_on_char ' ' g))) sets s
;;

let _print_cube = function
  | Red n -> Printf.printf "red %d, " n
  | Green n -> Printf.printf "green %d, " n
  | Blue n -> Printf.printf "blue %d, " n
;;

let rec _print_cubes = function
  | [] -> print_endline ""
  | c :: cs ->
    _print_cube c;
    _print_cubes cs
;;

let possible line =
  let id, sets = create_game 0 [] (String.split_on_char ':' line) in
  let gamemax = maxcolors (Red 0) (Green 0) (Blue 0) (List.flatten sets) in
  let zip = List.combine gamemax cubesmax in
  let greater = List.fold_right (fun (gm, cm) acc -> gm <= cm && acc) zip true in
  if greater then Some id else None
;;

let part1 filename =
  let ic = In_channel.open_text filename in
  let rec pgl acc =
    match In_channel.input_line ic with
    | Some line -> pgl (possible line :: acc)
    | _ -> List.fold_right ( + ) (List.filter_map (fun a -> a) acc) 0
  in
  pgl []
;;

let cube_count = function
  | Red n -> n
  | Green n -> n
  | Blue n -> n
;;

let gamepower line =
  let _, sets = create_game 0 [] (String.split_on_char ':' line) in
  let gamemax = maxcolors (Red 0) (Green 0) (Blue 0) (List.flatten sets) in
  List.fold_right (fun c acc -> cube_count c * acc) gamemax 1
;;

let part2 filename =
  let ic = In_channel.open_text filename in
  let rec pgl acc =
    match In_channel.input_line ic with
    | Some line -> pgl (gamepower line :: acc)
    | _ -> List.fold_right ( + ) acc 0
  in
  pgl []
;;

let () =
  Printf.printf "part 1:\n";
  print_int (part1 "input/day02.test");
  Printf.printf "\n";
  print_int (part1 "input/day02.input");
  Printf.printf "\n\npart 2:\n";
  print_int (part2 "input/day02.test");
  Printf.printf "\n";
  print_int (part2 "input/day02.input");
  Printf.printf "\n"
;;
