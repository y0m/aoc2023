type face =
  | Joker
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine
  | Ten
  | Jack
  | Queen
  | King
  | Ace

type hand =
  { cards : char list
  ; bid : int
  }

let face_of_char ?(joker = false) c =
  match c with
  | '2' -> Two
  | '3' -> Three
  | '4' -> Four
  | '5' -> Five
  | '6' -> Six
  | '7' -> Seven
  | '8' -> Eight
  | '9' -> Nine
  | 'T' -> Ten
  | 'J' -> if joker then Joker else Jack
  | 'Q' -> Queen
  | 'K' -> King
  | 'A' -> Ace
  | _ -> failwith "unknown face"
;;

type handkind =
  | Single
  | HighCard
  | Pair
  | Pair2
  | ThreeOAK
  | FullHouse
  | FourOAK
  | FiveOAK

let get_hk = function
  | c, 1 -> Single, c
  | c, 2 -> Pair, c
  | c, 3 -> ThreeOAK, c
  | c, 4 -> FourOAK, c
  | c, 5 -> FiveOAK, c
  | _ -> failwith "cannot resolve hand type"
;;

module FaceHash = struct
  type t = char

  let equal = Char.equal
  let hash = Hashtbl.hash
end

module FaceTbl = Hashtbl.Make (FaceHash)

module HandKindHash = struct
  type t = handkind

  let equal i j = i = j
  let hash = Hashtbl.hash
end

module HandKindTbl = Hashtbl.Make (HandKindHash)

let char_list_of_string s = s |> String.to_seq |> List.of_seq

let get_hand line =
  let data =
    List.filter_map
      (fun s -> if s <> "" then Some s else None)
      (String.split_on_char ' ' line)
  in
  let cards = char_list_of_string (List.nth data 0) in
  let bid = int_of_string (List.nth data 1) in
  { cards; bid }
;;

let get_hands filename =
  List.map (fun l -> get_hand l) In_channel.(with_open_text filename input_lines)
;;

let get_fhk tbl =
  let pair = List.length (HandKindTbl.find_opt tbl Pair |> Option.value ~default:[]) in
  let toak =
    List.length (HandKindTbl.find_opt tbl ThreeOAK |> Option.value ~default:[])
  in
  let foak = List.length (HandKindTbl.find_opt tbl FourOAK |> Option.value ~default:[]) in
  let foak' =
    List.length (HandKindTbl.find_opt tbl FiveOAK |> Option.value ~default:[])
  in
  if foak' = 1
  then FiveOAK
  else if foak = 1
  then FourOAK
  else if toak = 1 && pair = 1
  then FullHouse
  else if toak = 1
  then ThreeOAK
  else if pair = 2
  then Pair2
  else if pair = 1
  then Pair
  else HighCard
;;

let get_handtype h =
  let tbl = FaceTbl.create 5 in
  let _ =
    List.iter
      (fun c ->
        let count = FaceTbl.find_opt tbl c |> Option.value ~default:0 in
        FaceTbl.replace tbl c (count + 1))
      h.cards
  in
  let hk = List.map (fun i -> get_hk i) (FaceTbl.to_seq tbl |> List.of_seq) in
  let hktbl = HandKindTbl.create 5 in
  let _ =
    List.iter
      (fun (ht, c) ->
        let acc = HandKindTbl.find_opt hktbl ht |> Option.value ~default:[] in
        HandKindTbl.replace hktbl ht (c :: acc))
      hk
  in
  get_fhk hktbl
;;

let compare_hth ?(joker = false) hta ha htb hb =
  if hta < htb
  then -1
  else if hta > htb
  then 1
  else (
    let hab = List.combine ha.cards hb.cards in
    match
      List.find_map
        (fun (a, b) ->
          if face_of_char ~joker a < face_of_char ~joker b
          then Some (-1)
          else if face_of_char ~joker a > face_of_char ~joker b
          then Some 1
          else None)
        hab
    with
    | None -> 0
    | Some n -> n)
;;

let part1 filename =
  let hands = get_hands filename in
  let handstypes = List.map (fun h -> get_handtype h) hands in
  let tuples = List.combine handstypes hands in
  let results = List.sort (fun (hta, ha) (htb, hb) -> compare_hth hta ha htb hb) tuples in
  List.fold_left
    (fun acc (i, b) -> acc + (i * b))
    0
    (List.mapi (fun i (_, h) -> i + 1, h.bid) results)
;;

let get_fhk' h tbl =
  let joker = List.mem 'J' h.cards in
  let pair = HandKindTbl.find_opt tbl Pair |> Option.value ~default:[] in
  let toak = HandKindTbl.find_opt tbl ThreeOAK |> Option.value ~default:[] in
  let foak = HandKindTbl.find_opt tbl FourOAK |> Option.value ~default:[] in
  let foak' = HandKindTbl.find_opt tbl FiveOAK |> Option.value ~default:[] in
  if List.length foak' = 1
  then FiveOAK
  else if List.length foak = 1
  then if joker then FiveOAK else FourOAK
  else if List.length toak = 1 && List.length pair = 1
  then if joker then FiveOAK else FullHouse
  else if List.length toak = 1
  then if joker then FourOAK else ThreeOAK
  else if List.length pair = 2
  then if List.mem 'J' pair then FourOAK else if joker then FullHouse else Pair2
  else if List.length pair = 1
  then if joker then ThreeOAK else Pair
  else if joker
  then Pair
  else HighCard
;;

let get_handtype' h =
  let tbl = FaceTbl.create 5 in
  let _ =
    List.iter
      (fun c ->
        let count = FaceTbl.find_opt tbl c |> Option.value ~default:0 in
        FaceTbl.replace tbl c (count + 1))
      h.cards
  in
  let hk = List.map (fun i -> get_hk i) (FaceTbl.to_seq tbl |> List.of_seq) in
  let hktbl = HandKindTbl.create 5 in
  let _ =
    List.iter
      (fun (k, c) ->
        let acc = HandKindTbl.find_opt hktbl k |> Option.value ~default:[] in
        HandKindTbl.replace hktbl k (c :: acc))
      hk
  in
  get_fhk' h hktbl
;;

let part2 filename =
  let hands = get_hands filename in
  let handstypes = List.map (fun h -> get_handtype' h) hands in
  let tuples = List.combine handstypes hands in
  let results =
    List.sort (fun (hta, ha) (htb, hb) -> compare_hth ~joker:true hta ha htb hb) tuples
  in
  List.fold_left
    (fun acc (i, b) -> acc + (i * b))
    0
    (List.mapi (fun i (_, h) -> i + 1, h.bid) results)
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day07.test");
  Printf.printf " input: %d\n" (part1 "input/day07.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day07.test");
  Printf.printf " input: %d\n" (part2 "input/day07.input")
;;
