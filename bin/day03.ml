let char_list_of_string s = s |> String.to_seqi |> List.of_seq
let string_of_char = String.make 1

type number =
  { idx : int
  ; str : string
  ; start : int
  ; endp : int
  ; num : int
  }

type symbol =
  { idx : int
  ; c : char
  ; pos : int
  }

type token =
  | Number of number
  | Symbol of symbol
  | Nothing

let rec get_tokens acc idx prev_t = function
  | [] ->
    (match prev_t with
     | Symbol _ | Number _ -> prev_t :: acc
     | _ -> acc)
  | (p, c) :: tl ->
    let t =
      match c with
      | '0' .. '9' ->
        (match prev_t with
         | Number { idx = _; str = s; start = stp; endp = _; num = _ } ->
           Number { idx; str = s ^ string_of_char c; start = stp; endp = p; num = 0 }
         | _ -> Number { idx; str = string_of_char c; start = p; endp = p; num = 0 })
      | '.' -> Nothing
      | _ -> Symbol { idx; c; pos = p }
    in
    (match prev_t with
     | Number _ ->
       (match t with
        | Number _ -> get_tokens acc idx t tl
        | Nothing | Symbol _ -> get_tokens (prev_t :: acc) idx t tl)
     | Symbol _ -> get_tokens (prev_t :: acc) idx t tl
     | Nothing -> get_tokens acc idx t tl)
;;

let is_adj (s : symbol) (n : number) =
  let { idx = si; c = _sc; pos = sp } = s in
  let { idx = ni; str = _; start = nstart; endp = nend; num = _ } = n in
  (si = ni && (sp + 1 = nstart || sp - 1 = nend))
  || ((ni = si - 1 || ni = si + 1)
      && ((nstart <= sp && sp <= nend) || sp + 1 = nstart || sp - 1 = nend))
;;

let load_input filename test =
  let ic = In_channel.open_text filename in
  let rec read_line acc idx =
    match In_channel.input_line ic with
    | None -> acc
    | Some line ->
      let tokens = get_tokens [] idx Nothing (char_list_of_string line) in
      read_line (tokens :: acc) (idx + 1)
  in
  let tokens = List.flatten (read_line [] 0) in
  let numbers =
    List.filter_map
      (fun t ->
        match t with
        | Number { idx = i; str = s; start = stp; endp = ep; num = _ } ->
          Some { idx = i; str = s; start = stp; endp = ep; num = int_of_string s }
        | _ -> None)
      tokens
  in
  let symbols =
    List.filter_map
      (fun t ->
        match t with
        | Symbol s -> if test s then Some s else None
        | _ -> None)
      tokens
  in
  numbers, symbols
;;

let part1 filename =
  let numbers, symbols = load_input filename (fun _ -> true) in
  List.fold_right
    (fun (s : symbol) a ->
      a
      + List.fold_right
          (fun n b -> if is_adj s n then n.num + b else b)
          (List.filter_map
             (fun (nn : number) ->
               if s.idx - 1 <= nn.idx && nn.idx <= s.idx + 1 then Some nn else None)
             numbers)
          0)
    symbols
    0
;;

let part2 filename =
  let numbers, symbols = load_input filename (fun s -> s.c = '*') in
  List.fold_right
    (fun (s : symbol) a ->
      a
      +
      let ns =
        List.filter_map
          (fun (n : number) -> if is_adj s n then Some n else None)
          (List.filter_map
             (fun (nn : number) ->
               if s.idx - 1 <= nn.idx && nn.idx <= s.idx + 1 then Some nn else None)
             numbers)
      in
      if List.length ns = 2
      then List.fold_right (fun (n : number) b -> n.num * b) ns 1
      else 0)
    symbols
    0
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day03.test");
  Printf.printf " input: %d\n" (part1 "input/day03.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day03.test");
  Printf.printf " input: %d\n" (part2 "input/day03.input")
;;
