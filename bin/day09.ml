let load_input filename =
  List.map
    (fun l -> List.map int_of_string (String.split_on_char ' ' l))
    In_channel.(with_open_text filename input_lines)
;;

let rec list_last = function
  | [] -> 0
  | [ x ] -> x
  | _ :: tl -> list_last tl
;;

let rec ldiff acc hd = function
  | [] -> List.rev acc
  | b :: tl -> ldiff ((b - hd) :: acc) b tl
;;

let part1 filename =
  let vl = load_input filename in
  let rec extrapolate l =
    match List.for_all (Int.equal 0) l with
    | true -> List.hd l
    | false -> list_last l + (extrapolate @@ ldiff [] (List.hd l) (List.tl l))
  in
  List.fold_left ( + ) 0 (List.map extrapolate vl)
;;

let part2 filename =
  let vl = load_input filename in
  let rec extrapolate l =
    match List.for_all (Int.equal 0) l with
    | true -> List.hd l
    | false -> List.hd l - (extrapolate @@ ldiff [] (List.hd l) (List.tl l))
  in
  List.fold_left ( + ) 0 (List.map extrapolate vl)
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day09.test");
  Printf.printf " input: %d\n" (part1 "input/day09.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day09.test2");
  Printf.printf " input: %d\n" (part2 "input/day09.input")
;;
