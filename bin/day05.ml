type ft_map =
  { src : int
  ; dst : int
  ; rng : int
  }

type ranges =
  { _name : string
  ; maps : ft_map list
  }

let get_maps infos =
  let name = List.hd infos in
  let maps =
    List.map
      (fun s ->
        let values = Array.of_list (String.split_on_char ' ' s) in
        { src = int_of_string values.(1)
        ; dst = int_of_string values.(0)
        ; rng = int_of_string values.(2)
        })
      (List.tl infos)
  in
  { _name = name; maps }
;;

let rec get_ranges acc = function
  | [] -> acc
  | bl :: tl -> get_ranges (get_maps bl :: acc) tl
;;

let split_blocks lines =
  let rec loop ac1 ac2 = function
    | [] -> List.rev ac2 :: ac1
    | hd :: tl ->
      if hd = "" then loop (List.rev ac2 :: ac1) [] tl else loop ac1 (hd :: ac2) tl
  in
  loop [] [] lines
;;

let get_dst seed map =
  if map.src <= seed && seed < map.src + map.rng
  then Some (map.dst + (seed - map.src))
  else None
;;

let get_location seed ranges =
  let search_maps src maps =
    match List.find_map (fun m -> get_dst src m) maps with
    | None -> src
    | Some dst -> dst
  in
  let rec loop_ranges src = function
    | [] -> src
    | rng :: tl -> loop_ranges (search_maps src rng.maps) tl
  in
  loop_ranges seed ranges
;;

let part1 filename =
  let ic = In_channel.open_text filename in
  let firstline =
    match In_channel.input_line ic with
    | None -> failwith "can't be there yet"
    | Some line -> line
  in
  let seeds =
    List.map
      (fun s -> int_of_string s)
      (String.split_on_char
         ' '
         (String.trim (List.nth (String.split_on_char ':' firstline) 1)))
  in
  let _ = In_channel.input_line ic in
  let rngs = get_ranges [] (split_blocks (In_channel.input_lines ic)) in
  List.hd (List.sort compare (List.map (fun s -> get_location s rngs) seeds))
;;

let get_locations maps seed =
  let m = List.sort (fun ma mb -> Int.compare ma.src mb.src) maps.maps in
  let rec aux acc (s, e) =
    if s >= e
    then acc
    else (
      match
        List.find_opt
          (fun map -> if s < map.src then e > map.src else s < map.src + map.rng)
          m
      with
      | None -> (s, e) :: acc
      | Some map ->
        if s < map.src
        then aux ((s, map.src) :: acc) (map.src, e)
        else if e > map.src + map.rng
        then aux ((s - map.src + map.dst, map.dst + map.rng) :: acc) (map.src + map.rng, e)
        else (s - map.src + map.dst, s - map.src + map.dst + (e - s)) :: acc)
  in
  aux [] seed
;;

let part2 filename =
  let ic = In_channel.open_text filename in
  let firstline =
    match In_channel.input_line ic with
    | None -> failwith "can't be there yet"
    | Some line -> line
  in
  let seeds =
    let rec get_seeds acc = function
      | [] -> acc
      | src :: rng :: tl -> get_seeds ((int_of_string src, int_of_string rng) :: acc) tl
      | _ -> failwith "cannot get seeds"
    in
    let seedstrs =
      String.split_on_char
        ' '
        (String.trim (List.nth (String.split_on_char ':' firstline) 1))
    in
    List.rev (get_seeds [] seedstrs)
  in
  let _ = In_channel.input_line ic in
  let rngs = get_ranges [] (split_blocks (In_channel.input_lines ic)) in
  List.map
    (fun (ss, sr) ->
      List.fold_left
        (fun sa maps -> List.flatten (List.map (get_locations maps) sa))
        [ ss, ss + sr ]
        rngs)
    seeds
  |> List.flatten
  |> List.fold_left (fun acc (d, _) -> min acc d) Int.max_int
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day05.test");
  Printf.printf " input: %d\n" (part1 "input/day05.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day05.test");
  Printf.printf " input: %d\n" (part2 "input/day05.input")
;;
