let string_to_list s = s |> String.to_seq |> List.of_seq
let first_digit l = int_of_char (List.hd l) - int_of_char '0'
let last_digit l = first_digit (List.rev l)
let char_is_digit c = '0' <= c && c <= '9'

let extract_digits s =
  let l' = List.filter char_is_digit (string_to_list s) in
  (10 * first_digit l') + last_digit l'
;;

let part1 filename =
  let lines = In_channel.(with_open_text filename input_lines) in
  print_int (List.fold_right ( + ) (List.map extract_digits lines) 0)
;;

let sdigits =
  [ "one", 1
  ; "two", 2
  ; "three", 3
  ; "four", 4
  ; "five", 5
  ; "six", 6
  ; "seven", 7
  ; "eight", 8
  ; "nine", 9
  ]
;;

let rev_string x =
  let len = String.length x in
  let y = Bytes.create len in
  for i = 0 to (len / 2) - 1 do
    Bytes.unsafe_set y i (String.unsafe_get x (len - i - 1));
    Bytes.unsafe_set y (len - i - 1) (String.unsafe_get x i)
  done;
  if len land 1 = 1 then Bytes.unsafe_set y (len / 2) (String.unsafe_get x (len / 2));
  Bytes.unsafe_to_string y
;;

let list_to_string l = l |> List.to_seq |> String.of_seq

let rec get_digit f = function
  | [] -> 0
  | x :: xs as l ->
    (match x with
     | '0' .. '9' -> int_of_char x - int_of_char '0'
     | _ ->
       let s = list_to_string l in
       (match List.find_map (fun (p, d) -> if f s p then Some d else None) sdigits with
        | None -> get_digit f xs
        | Some d -> d))
;;

let first_digit' l = get_digit (fun s p -> String.starts_with ~prefix:p s) l

let last_digit' l =
  get_digit (fun s p -> String.starts_with ~prefix:(rev_string p) s) (List.rev l)
;;

let extract_digits' l = (10 * first_digit' l) + last_digit' l

let part2 filename =
  let lines = In_channel.(with_open_text filename input_lines) in
  print_int
    (List.fold_right
       ( + )
       (List.map (fun s -> extract_digits' (string_to_list s)) lines)
       0)
;;

let () =
  part1 "input/day01.test";
  print_endline "";
  part1 "input/day01.input";
  print_endline "";
  part2 "input/day01.test2";
  print_endline "";
  part2 "input/day01.input"
;;
