let f4 t d =
  let inc_acc i acc = if i * (t - i) > d then acc + 1 else acc in
  let rec range acc = function
    | 0 -> inc_acc 0 acc
    | n -> range (inc_acc n acc) (n - 1)
  in
  range 0 t
;;

let part1 f filename =
  let ls = In_channel.(with_open_text filename input_lines) in
  let data =
    List.map
      (fun l ->
        List.filter_map
          (fun e -> if e <> "" then Some (int_of_string e) else None)
          (String.split_on_char ' ' (List.nth (String.split_on_char ':' l) 1)))
      ls
  in
  let races = List.combine (List.nth data 0) (List.nth data 1) in
  List.fold_right (fun (t, d) acc -> acc * f t d) races 1
;;

let part2 filename =
  let ls = In_channel.(with_open_text filename input_lines) in
  let data =
    List.map
      (fun l ->
        List.filter_map
          (fun e -> if e <> "" then Some e else None)
          (String.split_on_char ' ' (List.nth (String.split_on_char ':' l) 1)))
      ls
  in
  let race =
    List.map (fun d -> int_of_string @@ List.fold_left (fun acc s -> acc ^ s) "" d) data
  in
  f4 (List.nth race 0) (List.nth race 1)
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 f4 "input/day06.test");
  Printf.printf " input: %d\n" (part1 f4 "input/day06.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day06.test");
  Printf.printf " input: %d\n" (part2 "input/day06.input")
;;
