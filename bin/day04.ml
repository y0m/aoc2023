let _power base exp =
  let rec pow = function
    | 0 -> 1
    | n -> base * pow (n - 1)
  in
  pow exp
;;

let get_points line =
  let card =
    String.split_on_char '|' (String.trim (List.nth (String.split_on_char ':' line) 1))
  in
  let winners =
    List.filter_map
      (fun s -> if s = String.empty then None else Some (int_of_string s))
      (String.split_on_char ' ' (String.trim (List.nth card 0)))
  in
  let numbers =
    List.filter_map
      (fun s -> if s = String.empty then None else Some (int_of_string s))
      (String.split_on_char ' ' (String.trim (List.nth card 1)))
  in
  let found =
    List.fold_right (fun n a -> if List.mem n winners then a + 1 else a) numbers 0
  in
  match found with
  | 0 -> 0
  | n -> _power 2 (n - 1)
;;

let part1 filename =
  let ic = In_channel.open_text filename in
  let rec loop acc =
    match In_channel.input_line ic with
    | None -> acc
    | Some line -> loop (get_points line :: acc)
  in
  let points = loop [] in
  List.fold_right ( + ) points 0
;;

let get_card_result line =
  let card =
    String.split_on_char '|' (String.trim (List.nth (String.split_on_char ':' line) 1))
  in
  let winners =
    List.filter_map
      (fun s -> if s = String.empty then None else Some (int_of_string s))
      (String.split_on_char ' ' (String.trim (List.nth card 0)))
  in
  let numbers =
    List.filter_map
      (fun s -> if s = String.empty then None else Some (int_of_string s))
      (String.split_on_char ' ' (String.trim (List.nth card 1)))
  in
  List.fold_right (fun n a -> if List.mem n winners then a + 1 else a) numbers 0
;;

let get_card_copies idx origs =
  let n = origs.(idx) in
  match n with
  | 0 -> []
  | _ -> Array.to_list (Array.init n (fun i -> idx + i + 1))
;;

let part2 filename =
  let lines = In_channel.(with_open_text filename input_lines) in
  let originals = Array.init (List.length lines) (fun _ -> 0) in
  let rec get_results i = function
    | [] -> ()
    | l :: ls ->
      originals.(i) <- get_card_result l;
      get_results (i + 1) ls
  in
  let _ = get_results 0 lines in
  let copies = Array.init (List.length lines) (fun _ -> 1) in
  let rec exhaust = function
    | [] -> ()
    | cards ->
      let rec loop acc = function
        | [] -> acc
        | x :: xs -> loop (get_card_copies x originals :: acc) xs
      in
      let turn = List.sort compare (List.flatten (loop [] cards)) in
      let _ = List.iter (fun c -> copies.(c) <- copies.(c) + 1) turn in
      exhaust turn
  in
  let _ = exhaust (Array.to_list @@ Array.init (List.length lines) (fun i -> i)) in
  Array.fold_right ( + ) copies 0
;;

let () =
  Printf.printf "Part 1:\n";
  Printf.printf " test: %d\n" (part1 "input/day04.test");
  Printf.printf " input: %d\n" (part1 "input/day04.input");
  Printf.printf "Part 2:\n";
  Printf.printf " test: %d\n" (part2 "input/day04.test");
  Printf.printf " test: %d\n" (part2 "input/day04.input")
;;
